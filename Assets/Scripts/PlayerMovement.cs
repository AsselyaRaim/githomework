using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _acceleration;
    [SerializeField] private float _maxSpeed;
    [SerializeField] private float _rotateSpeed;

    private Rigidbody _rigidBody;
    private Vector3 _velocity;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        _velocity = transform.forward * (vertical * _acceleration);

        float rotateValue = horizontal * _rotateSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, rotateValue);
    }

    private void FixedUpdate()
    {
        _rigidBody.AddForce(_velocity, ForceMode.Force);

        Vector3 xzVelocity = new Vector3(_rigidBody.velocity.x, 0f, _rigidBody.velocity.z);
        if (xzVelocity.magnitude > _maxSpeed)
        {
            xzVelocity = _rigidBody.velocity.normalized * _maxSpeed;
            _rigidBody.velocity = xzVelocity;
        }
    }
}

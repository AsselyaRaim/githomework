using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _startBtn;
    [SerializeField] private Button _exitBtn;

    private void Awake()
    {
        _startBtn.onClick.AddListener(OnStartBtnClicked);
        _exitBtn.onClick.AddListener(OnExitBtnClicked);
    }

    private void OnStartBtnClicked()
    {
        SceneManager.LoadScene(1);
    }

    private void OnExitBtnClicked()
    {
        Application.Quit();
    }

}
